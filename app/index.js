'use strict';

var yeoman = require('yeoman-generator');

module.exports = yeoman.generators.Base.extend({
    constructor: function() {
        yeoman.generators.Base.apply(this, arguments);
    },

    writing: {
        common: function() {
            this.fs.copy(
                this.templatePath('editorconfig'),
                this.destinationPath('.editorconfig')
            );

            this.fs.copy(
                this.templatePath('gitignore'),
                this.destinationPath('.gitignore')
            );

            this.fs.copy(
                this.templatePath('eslintrc'),
                this.destinationPath('.eslintrc')
            );

            this.fs.copy(
                this.templatePath('eslintignore'),
                this.destinationPath('.eslintignore')
            );

            this.fs.copy(
                this.templatePath('travis'),
                this.destinationPath('.travis.yml')
            );

            this.fs.copy(
                this.templatePath('LICENSE'),
                this.destinationPath('LICENSE')
            );

            this.fs.copy(
                this.templatePath('CONTRIBUTING.md'),
                this.destinationPath('CONTRIBUTING.md')
            );
        }
    }
});
