[![npm](https://img.shields.io/npm/v/@yummies/generator-yummies-repo.svg?style=flat-square)](https://www.npmjs.com/package/@yummies/generator-yummies-repo)
[![deps](http://img.shields.io/david/yummies/generator-yummies-repo.svg?style=flat-square)](https://david-dm.org/yummies/generator-yummies-repo)

Maintain repos common files with Yeoman.

### Install

```
npm i -g yo @yummies/generator-yummies-repo
```

### Usage

```
yo @yummies/yummies-repo
```
